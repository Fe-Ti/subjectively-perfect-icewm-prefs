#!/bin/bash

# recover max native resolution
CURRES=$(xrandr | sed -n 3p  | awk  '{print $1}')
FULLW="$(cut -d'x' -f1 <<< $CURRES)"
FULLH="$(cut -d'x' -f2 <<< $CURRES)"

# taskbar pixel height
TASKH=25 # hardcoded

# active window id
# ID="-r :ACTIVE:" # does not work as consistently as the call to xdotool
ID="-i -r `xdotool getwindowfocus`"

# disable maximized attribute
wmctrl $ID -b remove,maximized_vert
wmctrl $ID -b remove,maximized_horz

case "$1" in
    w)
        # tile left
        W=$(($FULLW / 2))
        H=$(($FULLH - 2 * $TASKH))
        X=0
        Y=$TASKH
        ;;
    e)
        # tile right
        W=$(($FULLW / 2))
        H=$(($FULLH - 2 * $TASKH))
        X=$W
        Y=$TASKH
        ;;
    n)
        # tile top
        W=$FULLW
        H=$((($FULLH - $TASKH) / 2))
        X=0
        Y=$TASKH
        ;;
    s)
        # tile bottom
        W=$FULLW
        H=$((($FULLH - $TASKH) / 2))
        X=0
        Y=$H
        ;;
esac

# resize
wmctrl $ID -e 0,$X,$Y,$W,$H
