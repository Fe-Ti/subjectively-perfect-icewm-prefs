#!/bin/bash
        CMD=$1
        if [ $2="--args" ]
        then
                CMD="$CMD $3"
        fi
#       echo $CMD
        if test $(pgrep -u $(id -u) $1)
        then
                # Firstly killing it
                kill $(pgrep -u $(id -u) $1)
                sleep 1
        fi
        # And then running
        $CMD&

